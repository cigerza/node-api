const express = require('express'); // importa o express
const cors = require('cors');
const mongoose = require('mongoose'); //importa o mongoose
const requireDir = require('require-dir'); // importa a biblioteca que gera os requires

// Iniciando o App
const app = express();
app.use(express.json());
app.use(cors());

// Iniciando o DB
mongoose.connect('mongodb://localhost:27017/nodeapi', { useNewUrlParser: true, useUnifiedTopology: true });
// require('.src/models/Product'); => registra o model na aplicação manualmente, um a um
requireDir('./src/models'); // usa a biblioteca para fazer os requires

// const Product = mongoose.model('Product');

// Primeira rota
// app.get('/', (req, res) => {   // get -> toda vez que o usuário acessar essa rota; '/' indica que é a rota raiz; req é a requisição que pode ser feita pelo usuário e deve conter as informações de todas as possibilidades; res é a resposta e deve ter todas as informações para dar uma resposta ao usuário
/*    Product.create({
        title: 'React Native',
        description: 'Build native apps with React',
        url: 'http://github.com/facebook/react-native'
    });

    return res.send('Hello Rocketseat');
});  => rota transferida para arquivo routes */

// Rotas
app.use("/api", require("./src/routes")); 

app.listen(3001); // porta 3001 do navegador, ou seja, poderá acessar a API pelo navegador digitando: localhost:3001


